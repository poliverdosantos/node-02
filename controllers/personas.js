const { response , request } = require('express')
const DataPersona = require('../data/data-person')
const Persona = require('../models/Persona')

const dataPerson = new DataPersona()

const personaGet = (req = request , res = response) => {

  const { nombres, apellidos} = req.query;
  if (nombres || apellidos) {
    let datosfilter = dataPerson.filtrarPersonas(nombres, apellidos);
    res.json({
      datosfilter
    })
  } else {
    let datos = dataPerson.getPersonas()
    res.json({
      datos
    })
  }
}

const personaGet2 = (req = request , res = response) => {

  const {parametro} = req.params

  if (parametro === "Masculino" || parametro === "Femenino") {
    const dato = dataPerson.data.filter(elemento => elemento.sexo === parametro)
    res.json({
      dato
    })
  } else {
    const dato2 = dataPerson.data.filter(elemento => elemento.ci === parametro)
    
    if (dato2.length !== 0) {
      res.json({
        dato2
      })
    } else {
      res.json({
        msg: "Introduce el ci o el sexo"
      })
    }
  }
}

const personaGet3 = (req = request , res = response) => {

  const {inicial, sexo} = req.params
  const dato = dataPerson.data.filter(elemento => elemento.sexo === sexo && elemento.nombres.charAt(0) === inicial)

  if (dato.length !== 0) {
    res.json({
      dato
    })
  } else {
    res.json({
      msg: "Datos no Encontrados"
    })
  }
}


const personaPost = (req = request, res = response) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  let personaNueva = new Persona(nombres, apellidos, ci, direccion, sexo)

  dataPerson.addPersona(personaNueva)

  res.json({
    personaNueva,
    msg: 'Persona añadida con exito'
  })
}

const personaPut = (req = request, res = response) => {
  const { id } = req.params
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const index = dataPerson.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPerson.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      nombres,
      apellidos,
      ci,
      direccion,
      sexo,
      msg: 'Persona Modificada con exito'
    })
  } else {
    res.json({
      msg: 'No encontrado'
    })
  }
}

const personaDelete = (req = request, res = response) => {
  const {id} = req.params
  const index = dataPerson.data.findIndex(object => object.id === id)
  if (index !== -1) {
    dataPerson.eliminarPersona(index)
    res.json({
      msg: 'La persona a sido eliminada con exito'
    })
  } else {
    res.json({
      msg: 'Persona no encontrada'
    })
  }
}

module.exports = {
  personaGet,
  personaGet2,
  personaGet3,
  personaPost,
  personaPut,
  personaDelete
}